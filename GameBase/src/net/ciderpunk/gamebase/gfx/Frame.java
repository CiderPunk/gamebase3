package net.ciderpunk.gamebase.gfx;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Frame extends Sprite {
	
	static Color tmpCol;
	
	public Frame(TextureRegion tx, float xOffs, float yOffs, float scale){
		super(tx);
		this.setOrigin(xOffs,yOffs);
		setScale(scale);
	}

	public Frame(TextureRegion tx, float xOffs, float yOffs){
		this(tx, xOffs, yOffs, 1f);
	}


	public Frame(TextureRegion tx, float xOffs, float yOffs, float scale, Color tint){
		this(tx, xOffs, yOffs,scale);
		this.setColor(tint);
	}

	public Frame(TextureRegion tx, float xOffs, float yOffs,  Color tint){
		this(tx, xOffs, yOffs, 1f, tint);
	}


	public Frame(TextureRegion tx, float xOffs, float yOffs, float scale, float alpha){
		this(tx, xOffs, yOffs, scale, new Color(0, 0, 0, alpha));
	}


	public void draw(SpriteBatch batch, float x, float y, float rot, Color col){
		tmpCol = this.getColor();
		this.setColor(col);
		this.draw(batch,x,y,rot);
		this.setColor(tmpCol);
	}
	
	public void  draw(SpriteBatch batch, float x, float y, float rot, float scale){
		if (scale != 1f){
			float tmpScale = this.getScaleX();
			this.setScale(scale);
			this.draw(batch, x, y, rot);
			this.setScale(tmpScale);
    }
    else{
			this.draw(batch, x, y, rot);
		}
	
	}
	
	public void draw(SpriteBatch batch, float x, float y, Color col){
		tmpCol = this.getColor();
		this.setColor(col);
		this.draw(batch,x,y);
		this.setColor(tmpCol);
	}
	
	public void draw(SpriteBatch batch, float x, float y){
		this.setPosition(x - this.getOriginX(), y - this.getOriginY());
		super.draw(batch);
	}
	
	public void draw(SpriteBatch batch, float x, float y, float rot){
		if (rot!=0f){
			this.setRotation(rot);
			this.draw(batch,x,y);
			this.setRotation(0);
		}
		else{
			this.draw(batch,x,y);
		}
	}
	
}
