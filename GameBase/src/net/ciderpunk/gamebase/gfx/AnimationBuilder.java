package net.ciderpunk.gamebase.gfx;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Matthew on 15/05/2014.
 */
public class AnimationBuilder {

  public static Animation buildAnim(TextureAtlas atlas, String name, float frameDelay, int xOff, int yOff){
    return buildAnim(atlas, name, frameDelay, xOff,yOff, 1f);

  }
  /**
   * builds an animation using the entire sequence as is
   * @param atlas
   * @param name
   * @param frameDelay
   * @param xOff
   * @param yOff
   * @return
   */
  public static Animation buildAnim(TextureAtlas atlas, String name, float frameDelay, int xOff, int yOff, float scale){
    Array<TextureAtlas.AtlasRegion> regions = atlas.findRegions(name);
    Frame[] frames = new Frame[regions.size];
    for(int i = 0; i < regions.size; i++){
      Frame frame = new Frame(regions.get(i), xOff, yOff);
      frame.setScale(scale);

      frames[i] = frame;

    }
    return new Animation(frameDelay, frames);
  }

  /**
   * builds an animation with the specified sequence of frames
   * @param atlas
   * @param name
   * @param frameDelay
   * @param xOff
   * @param yOff
   * @param sequence
   * @return
   */
  public static Animation buildAnim(TextureAtlas atlas, String name, float frameDelay, int xOff, int yOff, int[] sequence){
    Array<TextureAtlas.AtlasRegion> regions = atlas.findRegions(name);
    Frame[] frames = new Frame[sequence.length];
    for(int i = 0; i < sequence.length; i++){
      frames[i] = new Frame(regions.get(sequence[i]), xOff, yOff);
    }
    return new Animation(frameDelay, frames);
  }



}
