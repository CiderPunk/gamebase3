package net.ciderpunk.gamebase.events;

public interface IListener<T> {
	void doEvent(T source);
}
