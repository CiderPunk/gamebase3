package net.ciderpunk.gamebase.resources;

import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.PooledLinkedList;
import com.badlogic.gdx.assets.AssetManager;
import net.ciderpunk.gamebase.events.EventCaller;
import net.ciderpunk.gamebase.events.IEventCaller;
import net.ciderpunk.gamebase.events.IListener;
import net.ciderpunk.gamebase.game.GameManager;

public class ResourceManager implements Disposable, IEventCaller<ResourceManager> {


	EventCaller caller = new EventCaller();
	AssetManager assetMan;
	PooledLinkedList<IResourceUser> postLoadList;
	boolean loading;

	public AssetManager getAssetMan() {
		return assetMan;
	}

	public ResourceManager(){
		assetMan = new AssetManager();
		postLoadList = new PooledLinkedList<IResourceUser>(100);
	}

	public boolean update(){
		if (loading){
			if (assetMan.update()) {
				loading = false;
				postLoadList.iterReverse();
				IResourceUser item;
				while((item = postLoadList.previous()) !=null) {
					item.postLoad(this);
					postLoadList.remove();
				}
				if (!loading){
					caller.doEvent(this);
				}
			}
			return false;
		}
		return true;
	}

	public boolean isLoading(){
		return loading;
	}

	public void dispose(){
		assetMan.dispose();
	}

	public void addResourceUser(IResourceUser object){
		postLoadList.add(object);
		object.preLoad(this);
		loading = true;
	}

	public void addResourceUser(IResourceUser[] objects){
		for(IResourceUser object : objects){
			addResourceUser(object);
		}
	}

	@Override
	public ResourceManager addListener(IListener listener) {
		caller.addListener(listener);
		return this;
	}
}
