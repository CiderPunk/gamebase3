package net.ciderpunk.gamebase.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.sun.media.jfxmediaimpl.MediaDisposer;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;

/**
 * Created by matthewlander on 09/03/15.
 */
public abstract class GameManager implements IResourceUser, Disposable {

	protected ResourceManager resMan;
	protected Stage stage;
	protected Viewport viewport;
	protected SpriteBatch batch;


	public GameManager(Viewport vp){
		viewport = vp;
		resMan = new ResourceManager();
		resMan.addResourceUser(this);
		batch = new SpriteBatch();
		stage = new Stage(viewport, batch);
	}

	public GameManager(int width, int height){
		this(new FitViewport(width, height, new OrthographicCamera()));
	}

	public void dispose(){
		resMan.dispose();
		batch.dispose();
		stage.dispose();
	}

	public void update() {
		if (resMan.update()){
			float dt = Gdx.graphics.getDeltaTime();
			this.draw(dt);
			this.think(dt);
		}
	}

	protected void think(float dt){
		stage.act(dt);
	}

	public void draw(float dt){
		stage.draw();
	}

	public void resize(int width, int height){
		viewport.update(width, height, true);
	}
}
