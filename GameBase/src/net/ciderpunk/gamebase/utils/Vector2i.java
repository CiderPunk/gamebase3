package net.ciderpunk.gamebase.utils;

/**
 * Created by matthewlander on 16/05/15.
 */
public class Vector2i {
	public int x,y;
	public Vector2i(){}
	public Vector2i(Vector2i val){ set(val);}
	public Vector2i(int x, int y){ this.set(x,y); }
	public Vector2i set(int x, int y){
		this.x= x; this.y = y;
		return this;
	}
	public Vector2i set(Vector2i val){
		return this.set(val.x, val.y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result  = 21;
		result = prime * result + this.x;
		result = prime * result + this.y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if(obj == null || obj.getClass() != this.getClass()) return false;
		Vector2i vct = (Vector2i) obj;
		return vct.x == this.x && vct.y == this.y;
	}


}
