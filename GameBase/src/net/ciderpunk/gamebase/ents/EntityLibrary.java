package net.ciderpunk.gamebase.ents;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;

/**
 * Created by Matthew on 20/08/2015.
 */
public class EntityLibrary {

  private static final ObjectMap<String, Class> map = new ObjectMap<String, Class>();

  public static void RegisterClass(String name, Class clazz){
    if (!map.containsKey(name)){
      map.put(name, clazz);
    }
  }

  public static Object getObject(String name){
    Class clazz = map.get(name);
    if (clazz!= null) {
      try {
        return ClassReflection.newInstance(clazz);
      }
      catch(ReflectionException ex){
        Gdx.app.error("EntityLibrary", "Reflection error for class '" + name + "'", ex);
        return null;
      }
    }
    return null;
  }

}
