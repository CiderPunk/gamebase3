package net.ciderpunk.gamebase.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by matthewlander on 18/09/15.
 */
public interface IEntity {
	void cleanUp();
	boolean update(float dT);
	void draw(SpriteBatch batch);
	void debugDraw(ShapeRenderer renderer);
}
