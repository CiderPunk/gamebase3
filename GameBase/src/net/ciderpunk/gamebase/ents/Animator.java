package net.ciderpunk.gamebase.ents;

import com.badlogic.gdx.graphics.g2d.Animation;
import net.ciderpunk.gamebase.gfx.Frame;

/**
 * Created by matthewlander on 19/09/15.
 * controls an entites animaion
 */
public class Animator {
	protected float stateTime;
	protected Animation currentAnim;
	protected boolean loop;

	protected void startAnim(Animation anim) {
		startAnim(anim, true, 0f);
	}

	protected void startAnim(Animation anim, boolean loop) {
		startAnim(anim, loop, 0f);
	}

	protected void startAnim(Animation anim, boolean loop, float time){
		stateTime = time;
		currentAnim = anim;
		this.loop = loop;
	}

	public boolean isFinished(){
		return currentAnim.isAnimationFinished(this.stateTime);
	}

	public Frame update(float dT) {
		if (currentAnim != null){
			this.stateTime += dT;
			return (Frame)currentAnim.getKeyFrame(stateTime, loop);
		}
		return null;
	}
}
