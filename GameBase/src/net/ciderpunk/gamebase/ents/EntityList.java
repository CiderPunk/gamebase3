package net.ciderpunk.gamebase.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.PooledLinkedList;

/**
 * Created by matthewlander on 18/09/15.
 */
public class EntityList {

	protected PooledLinkedList<IEntity> mainBuffer;
	protected PooledLinkedList<IEntity> addBuffer;

	//static Vector2 tempV2 = new Vector2();
	//static Vector3 tempV3 = new Vector3();

	public EntityList(int mainBufferSize, int addBufferSize) {
		addBuffer = new PooledLinkedList<IEntity>(addBufferSize);
		mainBuffer = new PooledLinkedList<IEntity>(mainBufferSize);
	}

	public EntityList() {
		this(1000, 100);
	}

	public void clear() {
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			mainBuffer.remove();
			ent.cleanUp();
		}
		addBuffer.iter();
		while ((ent = addBuffer.next()) != null) {
			addBuffer.remove();
			ent.cleanUp();
		}

	}

	protected void consolidateList() {
		IEntity ent;
		addBuffer.iter();
		while ((ent = addBuffer.next()) != null) {
			mainBuffer.add(ent);
			addBuffer.remove();
		}
	}

	/**
	 * Performs update on all members of the list
	 *
	 * @param dT delta time
	 */
	public EntityList doUpdate(float dT) {
		this.consolidateList();
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			if (!ent.update(dT)) {
				mainBuffer.remove();
				ent.cleanUp();
			}
		}
		return this;
	}

	/**
	 * perform draw operation on all members of the list
	 *
	 * @param batch
	 */
	public void doDraw(SpriteBatch batch) {
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			ent.draw(batch);
		}
	}

	public void doDebugDraw(ShapeRenderer renderer) {
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			ent.debugDraw(renderer);
		}
	}
/*

/*
	public void doDraw(ModelBatch batch, Environment env) {
		Entity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			if (ent instanceof Entity3D) {
				((Entity3D) ent).draw(batch, env);
			}
		}
	}
*/
/*
	public Entity pointCollisonTest(Vector3 loc, float distance) {
		return pointCollisonTest(tempV2.set(loc.x, loc.y), distance);
	}
*/

	/*
	public Entity pointCollisonTest(Vector2 loc, float distance) {
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			if (ent.pointCollisionTest(loc, distance)) {
				return ent;
			}
		}
		return null;
	}


	public Entity rayIntersectTest(Ray ray){
		Entity ent;
		mainBuffer.iter();
		float bestDist = 0;
		Entity bestEnt = null;
		while ((ent = mainBuffer.next()) != null) {
			if (ent.rayIntersectTest(ray)){
				float dist = ray.origin.dst2(ent.getVect3Loc(tempV3));
				if (bestEnt == null || dist < bestDist){
					bestDist = dist;
					bestEnt = ent;
				}
			}
		}
		return bestEnt;
	}
*/

	public void add(IEntity arg0) {
		addBuffer.add(arg0);
	}

	public void remove(IEntity target) {
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			if (ent == target) {
				mainBuffer.remove();
				break;
			}
		}
	}

}
