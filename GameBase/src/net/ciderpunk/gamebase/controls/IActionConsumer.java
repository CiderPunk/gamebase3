package net.ciderpunk.gamebase.controls;

/**
 * Created by matthewlander on 13/10/15.
 */
public interface IActionConsumer {

	public boolean handleKeyDown(Action action);
	public boolean handleKeyUp(Action action);
}
